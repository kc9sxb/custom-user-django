from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _

from .managers import CustomUserManager

class CustomUser(AbstractUser):
    username = None
    email = models.EmailField(_('email address'), unique=True)

    firstName = models.CharField(max_length=200 , default = "")
    lastName = models.CharField(max_length=200 , default = "")
    organization = models.CharField(max_length=200 , default = "")
    billingStreet= models.CharField(max_length=200 , default = "")
    billingStreet2 = models.CharField(max_length=200 , default = "")
    billingCity = models.CharField(max_length=200 , default = "")
    billingProvince = models.CharField(max_length=200 , default = "")
    billingCode = models.CharField(max_length=200 , default = "")
    billingCountry = models.CharField(max_length=200 , default = "")
    

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        return self.email

