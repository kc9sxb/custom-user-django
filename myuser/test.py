from django.contrib.auth import get_user_model
from django.test import TestCase


class MyusersManagersTests(TestCase):

    def test_create_user(self):
        Myuser = get_user_model()
        myuser = Myuser.objects.create_user(email='normal@user.com', password='foo')
        self.assertEqual(myuser.email, 'normal@user.com')
        self.assertTrue(myuser.is_active)
        self.assertFalse(myuser.is_staff)
        self.assertFalse(myuser.is_superuser)
        try:
            # username is None for the AbstractUser option
            # username does not exist for the AbstractBaseUser option
            self.assertIsNone(myuser.username)
        except AttributeError:
            pass
        with self.assertRaises(TypeError):
            Myuser.objects.create_user()
        with self.assertRaises(TypeError):
            Myuser.objects.create_user(email='')
        with self.assertRaises(ValueError):
            Myuser.objects.create_user(email='', password="foo")

    def test_create_superuser(self):
        Myuser = get_user_model()
        admin_user = Myuser.objects.create_superuser(email='super@user.com', password='foo')
        self.assertEqual(admin_user.email, 'super@user.com')
        self.assertTrue(admin_user.is_active)
        self.assertTrue(admin_user.is_staff)
        self.assertTrue(admin_user.is_superuser)
        try:
            # username is None for the AbstractUser option
            # username does not exist for the AbstractBaseUser option
            self.assertIsNone(admin_user.username)
        except AttributeError:
            pass
        with self.assertRaises(ValueError):
            Myuser.objects.create_superuser(
                email='super@user.com', password='foo', is_superuser=False)

